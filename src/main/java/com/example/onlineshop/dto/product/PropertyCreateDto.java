package com.example.onlineshop.dto.product;

import com.example.onlineshop.dto.Dto;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * DTO for {@link com.example.onlineshop.domain.product.Property}
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PropertyCreateDto implements Serializable, Dto {

    @NotNull
    private String name;

    @NotNull
    private String value;
}
package com.example.onlineshop.dto.product;

import com.example.onlineshop.dto.Dto;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * DTO for {@link com.example.onlineshop.domain.product.Property}
 */

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class PropertyUpdateDto implements Serializable, Dto {

    @NotNull
    private String name;

    @NotNull
    private String value;

}

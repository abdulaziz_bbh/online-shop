package com.example.onlineshop.dto.product;

import com.example.onlineshop.dto.Dto;
import com.example.onlineshop.enums.State;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * DTO for {@link com.example.onlineshop.domain.product.Property}
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PropertyDto implements Serializable, Dto {

    @NotNull
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String value;

    private LocalDateTime createdDate;

    private LocalDateTime updatedDate;

    private Long createdBy;

    private Long updatedBy;

    private State state;
}

package com.example.onlineshop.dto.product;

import com.example.onlineshop.dto.Dto;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * DTO for {@link com.example.onlineshop.domain.product.Product}
 */

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ProductCreateDto implements Dto, Serializable {

    @NotNull
    private String name;

    @NotNull
    private Integer amount;

    @NotNull
    private Double price;

    @NotNull
    private String description;

    private Long categoryId;

    private Long brandId;
}

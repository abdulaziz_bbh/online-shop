package com.example.onlineshop.dto.product;

import com.example.onlineshop.dto.Dto;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * DTO for {@link com.example.onlineshop.domain.product.Brand}
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BrandCreateDto implements Dto, Serializable {

    @NotNull
    private String name;

    @NotNull
    private String description;


}

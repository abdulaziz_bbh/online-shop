package com.example.onlineshop.dto.product;

import com.example.onlineshop.dto.Dto;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

 /**
 * Dto for {@link com.example.onlineshop.domain.product.Brand}
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BrandUpdateDto implements Dto {

    @NotNull
    private String name;

    @NotNull
    private String description;
}

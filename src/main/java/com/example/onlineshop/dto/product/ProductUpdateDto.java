package com.example.onlineshop.dto.product;

import com.example.onlineshop.dto.Dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.io.Serializable;

/**
 * DTO for {@link com.example.onlineshop.domain.product.Product}
 */

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ProductUpdateDto implements Dto, Serializable {

    private String name;

    private Integer amount;

    private Double price;

    private String description;

    private Long categoryId;

    private Long brandId;


}

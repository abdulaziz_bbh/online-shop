package com.example.onlineshop.dto.product;

import com.example.onlineshop.domain.product.Brand;
import com.example.onlineshop.domain.product.Category;
import com.example.onlineshop.domain.product.Image;
import com.example.onlineshop.domain.product.Property;
import com.example.onlineshop.dto.Dto;
import com.example.onlineshop.enums.State;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * DTO for {@link com.example.onlineshop.domain.product.Product}
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto implements Dto, Serializable {

    @NotNull
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Category category;

    @NotNull
    private Brand brand;

    private List<Property> properties;

    private List<Image> images;

    @NotNull
    private Integer amount;

    @NotNull
    private Double price;

    @NotNull
    private String description;

    private LocalDateTime createdDate;

    private LocalDateTime updatedDate;

    private Long createdBy;

    private Long updatedBy;

    private State state;
}

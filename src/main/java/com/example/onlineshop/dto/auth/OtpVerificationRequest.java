package com.example.onlineshop.dto.auth;

import com.example.onlineshop.dto.Dto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OtpVerificationRequest implements Dto {

    private String email;

    private Integer otp;
}

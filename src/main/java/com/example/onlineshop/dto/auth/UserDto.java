package com.example.onlineshop.dto.auth;

import com.example.onlineshop.domain.auth.Role;
import com.example.onlineshop.dto.Dto;
import com.example.onlineshop.enums.State;
import com.example.onlineshop.util.MessageConstant;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * DTO for {@link com.example.onlineshop.domain.auth.User}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto implements Dto {

    private Long id;

    @NotNull(message = MessageConstant.THIS_FIELD_CANNOT_BE_NULL)
    private String firstName;

    @NotNull(message = MessageConstant.THIS_FIELD_CANNOT_BE_NULL)
    private String lastName;

    @NotNull(message = MessageConstant.THIS_FIELD_CANNOT_BE_NULL)
    private String email;

    private LocalDateTime createdDate;

    private LocalDateTime updatedDate;

    private Long createdBy;

    private Long updatedBy;

    private State state;

    private Role role;
}

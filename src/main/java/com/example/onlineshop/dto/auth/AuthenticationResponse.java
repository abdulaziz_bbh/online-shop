package com.example.onlineshop.dto.auth;

import com.example.onlineshop.dto.Dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationResponse implements Dto {

    private String accessToken;

    private String refreshToken;
}

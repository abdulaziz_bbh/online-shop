package com.example.onlineshop.dto.auth;

import com.example.onlineshop.dto.Dto;
import com.example.onlineshop.util.MessageConstant;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;

/**
 * DTO for {@link com.example.onlineshop.domain.auth.User}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserUpdateDto implements Dto {

    @NotNull(message = MessageConstant.THIS_FIELD_CANNOT_BE_NULL)
    private String firstName;

    @NotNull(message = MessageConstant.THIS_FIELD_CANNOT_BE_NULL)
    private String lastName;
}

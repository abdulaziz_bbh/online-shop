package com.example.onlineshop.dto.auth;

import com.example.onlineshop.dto.Dto;
import com.example.onlineshop.util.MessageConstant;
import com.example.onlineshop.validation.PasswordMatches;
import com.example.onlineshop.validation.ValidEmail;
import com.example.onlineshop.validation.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.NotNull;

/**
 * DTO for {@link com.example.onlineshop.domain.auth.User}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@PasswordMatches
public class UserCreateDto implements Dto {

    @NotNull(message = MessageConstant.THIS_FIELD_CANNOT_BE_NULL)
    private String firstName;

    @NotNull(message = MessageConstant.THIS_FIELD_CANNOT_BE_NULL)
    private String lastName;

    @NotNull(message = MessageConstant.THIS_FIELD_CANNOT_BE_NULL)
    @ValidEmail
    private String email;

    @NotNull(message = MessageConstant.THIS_FIELD_CANNOT_BE_NULL)
    @ValidPassword
    private String password;

    @NotNull(message = MessageConstant.THIS_FIELD_CANNOT_BE_NULL)
    private String prePassword;
}

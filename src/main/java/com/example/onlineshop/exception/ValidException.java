package com.example.onlineshop.exception;

public class ValidException extends GenericRuntimeException{
    
    public ValidException(String message) {
        super(message);
    }

    public ValidException(String message, String key) {
        super(message, key);
    }

    public ValidException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidException(String message, Throwable cause, String key) {
        super(message, cause, key);
    }
}

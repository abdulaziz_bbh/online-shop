package com.example.onlineshop.exception;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.FieldDefaults;

@EqualsAndHashCode(callSuper = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RestException extends RuntimeException{

    String message;

    public RestException(String message) {
        super(message);
        this.message = message;
    }

    public static RestException restThrow(String message){
        return  new RestException(message);
    }

}

package com.example.onlineshop.exception;

import com.example.onlineshop.response.ErrorData;
import com.example.onlineshop.response.ResponseData;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice

public class GlobalExceptionHandler{

    Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ResponseData<ErrorData>> handleException(MethodArgumentNotValidException exception, WebRequest request){
        logger.error(exception.getMessage(), exception);
        List<ErrorData> errors = new ArrayList<>();
        List<FieldError> fieldErrors  = exception.getBindingResult().getFieldErrors();
        fieldErrors.forEach(fieldError -> errors.add(new ErrorData(
                fieldError.getDefaultMessage(),
                fieldError.getField(),
                HttpStatus.BAD_REQUEST.value(),
                LocalDateTime.now(),
                request.getDescription(false))));
                return new ResponseEntity<>(ResponseData.errorResponse(errors), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = PersistenceException.class)
    public ResponseEntity<ResponseData<ErrorData>> handleException(PersistenceException exception, WebRequest request){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(request.getDescription(false),exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(value = ValidException.class)
    public ResponseEntity<ResponseData<ErrorData>> handleException(ValidException exception,WebRequest request){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(request.getDescription(false),exception.getMessage(), HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<ResponseData<ErrorData>> handleExcpetion(NotFoundException exception, WebRequest request){
        return new ResponseEntity<>(ResponseData.errorResponse(request.getDescription(false),exception.getMessage(), HttpStatus.NOT_FOUND.value()), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(value = NoResultException.class)
    public ResponseEntity<ResponseData<ErrorData>> handleException(NoResultException exception,WebRequest request){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(request.getDescription(false),exception.getMessage(), HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(value = RestException.class)
    public ResponseEntity<ResponseData<ErrorData>> handleException(RestException exception, WebRequest request){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(request.getDescription(false),exception.getMessage(), HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
    }



}

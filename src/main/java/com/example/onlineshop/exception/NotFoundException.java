package com.example.onlineshop.exception;

public class NotFoundException extends GenericRuntimeException{

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, String key) {
        super(message, key);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(String message, Throwable cause, String key) {
        super(message, cause, key);
    }
}

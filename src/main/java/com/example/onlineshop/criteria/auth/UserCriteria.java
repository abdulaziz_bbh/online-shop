package com.example.onlineshop.criteria.auth;

import com.example.onlineshop.criteria.GenericCriteria;
import lombok.*;

/**
 * Criteria for {@link com.example.onlineshop.domain.auth.User}
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserCriteria extends GenericCriteria {

    private String firstName;

    private String lastName;

    private String email;


    @Builder(builderMethodName = "childBuilder")
    public UserCriteria(Long id, Integer page, Integer prePage, String sortBy, String sortDirection, String firstName, String lastName, String email){
        super(id, page, prePage, sortBy, sortDirection);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
}

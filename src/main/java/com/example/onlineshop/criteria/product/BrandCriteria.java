package com.example.onlineshop.criteria.product;

import com.example.onlineshop.criteria.GenericCriteria;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Criteria for {@link com.example.onlineshop.domain.product.Brand}
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BrandCriteria extends GenericCriteria {

    private String name;

}

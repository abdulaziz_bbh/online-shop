package com.example.onlineshop.criteria.product;

import com.example.onlineshop.criteria.GenericCriteria;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProductCriteria extends GenericCriteria {

    private String name;

    private Double price;

    private Integer amount;

    private String brandName;

    private String categoryName;


}

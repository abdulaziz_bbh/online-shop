package com.example.onlineshop.criteria;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class GenericCriteria implements Criteria{

    protected Long id;

    protected Integer page;

    protected Integer perPage;

    protected String sortBy;

    protected String sortDirection;

    public String getSortDirection(){
        return sortDirection == null || sortDirection.isEmpty() ? "ASC" : sortDirection;
    }
}

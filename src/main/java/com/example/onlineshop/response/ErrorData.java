package com.example.onlineshop.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorData {

    String errorMessage;

    String fieldName;

    Integer errorCode;

    LocalDateTime timestamp;

    String path;

    public ErrorData(String path, String errorMessage, Integer errorCode){
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
        this.path = path;
        this.timestamp = LocalDateTime.now();
    }

    public ErrorData(String message, Integer errorCode) {
        this.errorMessage = message;
        this.errorCode = errorCode;
    }
}

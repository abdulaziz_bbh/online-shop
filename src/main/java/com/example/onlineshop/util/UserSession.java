package com.example.onlineshop.util;


import com.example.onlineshop.domain.auth.User;
import com.example.onlineshop.dto.auth.UserDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class UserSession {

    public UserDto getUser(){
        UserDto user = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication !=null){
            if(authentication.getPrincipal() instanceof User){
                user = (UserDto) authentication.getPrincipal();
            }
        }
        return user;
    }
}

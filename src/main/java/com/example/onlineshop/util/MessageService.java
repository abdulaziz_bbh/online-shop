package com.example.onlineshop.util;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MessageService {

    private static MessageSource messageSource;

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        MessageService.messageSource = messageSource;
    }

    public static String getMessage(String key){
        try {
            return messageSource.getMessage(key, null, LocaleContextHolder.getLocale());
        }catch (Exception e){
            return key;
        }
    }
}

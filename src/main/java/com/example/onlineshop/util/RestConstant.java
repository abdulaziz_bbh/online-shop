package com.example.onlineshop.util;

public interface RestConstant {

    String EMAIL_REGEX = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

    String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$";

    String TOKEN_TYPE = "Bearer ";

    String AUTHORIZATION_HEADER = "Authorization";

    String BASE_PATH_V1 = "/api/v1";

    String SIGN_UP_PATH = "sign-up";

    String ADMIN_CONTACT = "abdulaziz.hikmatov2001@gmail.com";


}

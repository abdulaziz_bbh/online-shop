package com.example.onlineshop.util;

import com.example.onlineshop.domain.auth.Role;
import com.example.onlineshop.domain.auth.User;
import com.example.onlineshop.enums.State;
import com.example.onlineshop.repository.auth.IUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final IUserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Value("${spring.sql.init.mode}")
    private String initialMode;

    @Override
    @Transactional
    public void run(String... args){

        if (initialMode.equals("always"))
            saveInitialRoleAndUser();
    }

    public void saveInitialRoleAndUser(){

        User admin = User.builder()
                .firstName("Admin")
                .lastName("Admin")
                .email(RestConstant.ADMIN_CONTACT)
                .state(State.NEW)
                .role(Role.ADMIN)
                .password(passwordEncoder.encode("root123"))
                .build();

        userRepository.save(admin);
    }

}

package com.example.onlineshop.mapper.product;

import com.example.onlineshop.domain.product.Brand;
import com.example.onlineshop.dto.product.BrandCreateDto;
import com.example.onlineshop.dto.product.BrandDto;
import com.example.onlineshop.dto.product.BrandUpdateDto;
import com.example.onlineshop.mapper.BaseMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

/**
 * Mapper for {@link Brand}
 */
@Mapper(componentModel = "spring")
@Component
public abstract class BrandMapper implements BaseMapper<
        Brand,
        BrandDto,
        BrandCreateDto,
        BrandUpdateDto> {

    @Override
    @Mapping(target = "state", expression = "java((com.example.onlineshop.enums.State.NEW))")
    public abstract Brand fromCreateDto(BrandCreateDto createDto);
}

package com.example.onlineshop.mapper.product;

import com.example.onlineshop.domain.product.Product;
import com.example.onlineshop.dto.product.ProductCreateDto;
import com.example.onlineshop.dto.product.ProductDto;
import com.example.onlineshop.dto.product.ProductUpdateDto;
import com.example.onlineshop.exception.NotFoundException;
import com.example.onlineshop.mapper.BaseMapper;
import com.example.onlineshop.repository.product.IBrandRepository;
import com.example.onlineshop.repository.product.ICategoryRepository;
import com.example.onlineshop.util.MessageConstant;
import com.example.onlineshop.util.MessageService;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Mapper for {@link Product}
 */
@Mapper(componentModel = "spring",
        imports = {ICategoryRepository.class, IBrandRepository.class}
)
@Component
public abstract class ProductMapper implements BaseMapper<
        Product,
        ProductDto,
        ProductCreateDto,
        ProductUpdateDto> {

    @Autowired
    ICategoryRepository categoryRepository;

    @Autowired
    IBrandRepository brandRepository;

    @Override
    @Mapping(target = "brand", expression = """
            java(brandRepository.find(createDto.getBrandId())
                            .orElseThrow(
                                    () -> new com.example.onlineshop.exception.NotFoundException(
                                            com.example.onlineshop.util.MessageService.getMessage(
                                                    com.example.onlineshop.util.MessageConstant.BRAND_NOT_FOUND))))""")

    @Mapping(target = "category", expression = """
            java(categoryRepository.find(createDto.getCategoryId())
                            .orElseThrow(
                                    () -> new com.example.onlineshop.exception.NotFoundException(
                                            com.example.onlineshop.util.MessageService.getMessage(
                                                    com.example.onlineshop.util.MessageConstant.CATEGORY_NOT_FOUND))))""")

    @Mapping(target = "state", expression = "java((com.example.onlineshop.enums.State.NEW))")
    public abstract Product fromCreateDto(ProductCreateDto createDto);

    @Override
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    public  Product fromUpdateDto(ProductUpdateDto updateDto, Product entity) {
        if (updateDto == null) {
            return entity;
        }

        if (updateDto.getName() != null) {
            entity.setName(updateDto.getName());
        }
        if (updateDto.getAmount() != null) {
            entity.setAmount(updateDto.getAmount());
        }
        if (updateDto.getPrice() != null) {
            entity.setPrice(updateDto.getPrice());
        }
        if (updateDto.getDescription() != null) {
            entity.setDescription(updateDto.getDescription());
        }
        if (updateDto.getBrandId() != null) {
            entity.setBrand(brandRepository.find(updateDto.getBrandId())
                    .orElseThrow(() -> new NotFoundException(
                            MessageService.getMessage(
                                    MessageConstant.BRAND_NOT_FOUND))));
        }
        if (updateDto.getCategoryId() != null) {
            entity.setCategory(categoryRepository.find(updateDto.getCategoryId())
                    .orElseThrow(() -> new NotFoundException(
                            MessageService.getMessage(
                                    MessageConstant.CATEGORY_NOT_FOUND))));
        }
        return entity;
    }

}

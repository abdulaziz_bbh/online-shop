package com.example.onlineshop.mapper.product;

import com.example.onlineshop.domain.product.Category;
import com.example.onlineshop.dto.product.CategoryCreateDto;
import com.example.onlineshop.dto.product.CategoryDto;
import com.example.onlineshop.dto.product.CategoryUpdateDto;
import com.example.onlineshop.mapper.BaseMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

/**
 * Mapper for {@link Category}
 */
@Mapper(componentModel = "spring")
@Component
public abstract class CategoryMapper implements BaseMapper<
        Category,
        CategoryDto,
        CategoryCreateDto,
        CategoryUpdateDto> {

    @Override
    @Mapping(target = "state", expression = "java((com.example.onlineshop.enums.State.NEW))")
    public abstract Category fromCreateDto(CategoryCreateDto createDto) ;
}

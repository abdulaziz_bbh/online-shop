package com.example.onlineshop.mapper.product;

import com.example.onlineshop.domain.product.Image;
import com.example.onlineshop.dto.product.ImageDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(
        componentModel = "spring"
)
public abstract class ImageMapper{

    public abstract ImageDto toDto(Image image);
}

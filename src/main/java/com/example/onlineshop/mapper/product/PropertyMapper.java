package com.example.onlineshop.mapper.product;

import com.example.onlineshop.domain.product.Property;
import com.example.onlineshop.dto.product.PropertyCreateDto;
import com.example.onlineshop.dto.product.PropertyDto;
import com.example.onlineshop.dto.product.PropertyUpdateDto;
import com.example.onlineshop.enums.State;
import com.example.onlineshop.mapper.BaseMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

/**
 * Mapper for {@link Property}
 */
@Mapper(
        componentModel = "spring"
)
@Component
public abstract class PropertyMapper implements BaseMapper<
        Property,
        PropertyDto,
        PropertyCreateDto,
        PropertyUpdateDto> {

    @Override
    @Mapping(target = "state", expression = "java((com.example.onlineshop.enums.State.NEW))")
    public abstract Property fromCreateDto(PropertyCreateDto createDto);
}
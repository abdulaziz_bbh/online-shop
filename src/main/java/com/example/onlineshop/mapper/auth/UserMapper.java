package com.example.onlineshop.mapper.auth;

import com.example.onlineshop.domain.auth.User;
import com.example.onlineshop.dto.auth.UserCreateDto;
import com.example.onlineshop.dto.auth.UserDto;
import com.example.onlineshop.dto.auth.UserUpdateDto;
import com.example.onlineshop.mapper.BaseMapper;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

@Mapper(
        componentModel = "spring",
        imports = {PasswordEncoder.class}
)
@RequiredArgsConstructor
public abstract class UserMapper implements BaseMapper<
        User,
        UserDto,
        UserCreateDto,
        UserUpdateDto> {

    @Autowired
    PasswordEncoder passwordEncoder;
    

    @Override
    public abstract User fromDto(UserDto dto);

    @Override
    @Mapping(target = "password", expression = "java(passwordEncoder.encode(createDto.getPassword()))")
    public abstract User fromCreateDto(UserCreateDto createDto);

    @Override
    public abstract UserDto toDto(User user);
}

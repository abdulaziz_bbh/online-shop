package com.example.onlineshop.mapper;

import com.example.onlineshop.domain.BaseDomain;
import com.example.onlineshop.dto.Dto;
import org.mapstruct.*;

import java.lang.annotation.Target;
import java.util.List;

/**
 * @param <E> Entity
 * @param <D> Dto
 * @param <CR> CreateDto
 * @param <UP> UpdateDto
 */

public interface BaseMapper<E extends BaseDomain, D extends Dto, CR extends Dto, UP extends Dto> {

    D toDto(E entity);

    E fromDto(D dto);

    List<D> toDto(List<E> entityList);

    List<E> fromtDto(List<D> dtoList);

    E fromCreateDto(CR createDto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    E fromUpdateDto(UP updateDto, @MappingTarget E entity);
}

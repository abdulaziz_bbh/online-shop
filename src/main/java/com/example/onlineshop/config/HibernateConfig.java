package com.example.onlineshop.config;


import com.example.onlineshop.util.UserSession;
import jakarta.persistence.EntityManagerFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.Optional;

@Configuration
@RequiredArgsConstructor
@EnableJpaAuditing
public class HibernateConfig implements AuditorAware<Long> {

    private final UserSession userSession;

    @Override
    public Optional<Long> getCurrentAuditor() {
        return Optional.ofNullable(
                userSession.getUser() != null ? userSession.getUser().getId() : null
        );
    }
    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
}

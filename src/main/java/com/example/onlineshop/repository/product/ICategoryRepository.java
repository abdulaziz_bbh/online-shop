package com.example.onlineshop.repository.product;

import com.example.onlineshop.criteria.product.CategoryCriteria;
import com.example.onlineshop.domain.product.Category;
import com.example.onlineshop.repository.IGenericRepository;

public interface ICategoryRepository extends IGenericRepository<Category, CategoryCriteria> {

    public boolean existCategoryByName(String name);

}

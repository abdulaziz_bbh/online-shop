package com.example.onlineshop.repository.product.impl;

import com.example.onlineshop.criteria.product.BrandCriteria;
import com.example.onlineshop.domain.product.Brand;
import com.example.onlineshop.repository.GenericDao;
import com.example.onlineshop.repository.product.IBrandRepository;
import com.example.onlineshop.util.BaseUtils;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Repository for {@link Brand}
 */
@Repository
public class BrandRepository extends GenericDao<Brand, BrandCriteria> implements IBrandRepository {

    @PersistenceContext
    private final EntityManager entityManager;


    protected BrandRepository(BaseUtils utils, EntityManager entityManager) {
        super(utils);
        this.entityManager = entityManager;
    }

    @Override
    protected void defineCriteriaOnQuery(
            BrandCriteria criteria,
            List<String> whereCause,
            Map<String, Object> params,
            StringBuilder queryBuilder) {
        if (!utils.isEmpty(criteria.getId())){
            whereCause.add("t.id = :id");
            params.put("id", criteria.getId());
        }
        if (!utils.isEmpty(criteria.getName())){
            whereCause.add("t.name = :name");
            params.put("name", criteria.getName());
        }
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }

    @Override
    public boolean existByBrandName(String name) {
        return (boolean)entityManager.createQuery("select coalesce((select exists (select 1 from Brand b where b.name = :name) as exist_row from Brand group by exist_row), false)")
                .setParameter("name", name)
                .getSingleResult();
    }
}

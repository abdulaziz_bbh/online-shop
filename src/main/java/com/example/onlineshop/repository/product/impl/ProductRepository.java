package com.example.onlineshop.repository.product.impl;

import com.example.onlineshop.criteria.product.ProductCriteria;
import com.example.onlineshop.domain.product.Product;
import com.example.onlineshop.repository.GenericDao;
import com.example.onlineshop.repository.product.IProductRepository;
import com.example.onlineshop.util.BaseUtils;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 * Repository for {@link Product}
 */
@Repository
public class ProductRepository extends GenericDao<Product, ProductCriteria> implements IProductRepository {

    @PersistenceContext
    private final EntityManager entityManager;

    protected ProductRepository(BaseUtils utils, EntityManager entityManager) {
        super(utils);
        this.entityManager = entityManager;
    }
}

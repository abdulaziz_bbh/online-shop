package com.example.onlineshop.repository.product;

import com.example.onlineshop.criteria.product.PropertyCriteria;
import com.example.onlineshop.domain.product.Property;
import com.example.onlineshop.repository.IGenericRepository;

public interface IPropertyRepository extends IGenericRepository<Property, PropertyCriteria> {

}

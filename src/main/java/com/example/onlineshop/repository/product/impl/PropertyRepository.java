package com.example.onlineshop.repository.product.impl;

import com.example.onlineshop.criteria.product.PropertyCriteria;
import com.example.onlineshop.domain.product.Property;
import com.example.onlineshop.repository.GenericDao;
import com.example.onlineshop.repository.product.IPropertyRepository;
import com.example.onlineshop.util.BaseUtils;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Repository for {@link com.example.onlineshop.domain.product.Property}
 */
@Repository
public class PropertyRepository extends GenericDao<Property, PropertyCriteria> implements IPropertyRepository {

    @PersistenceContext
    private final EntityManager entityManager;

    protected PropertyRepository(BaseUtils utils, EntityManager entityManager) {
        super(utils);
        this.entityManager = entityManager;
    }

    @Override
    protected void defineCriteriaOnQuery(
            PropertyCriteria criteria,
            List<String> whereCause,
            Map<String, Object> params,
            StringBuilder queryBuilder) {

        if (criteria.getId() != null){
            whereCause.add("t.id = :id");
            params.put("id", criteria.getId());
        }
        if (criteria.getName() != null){
            whereCause.add("t.name = :name");
            params.put("name", criteria.getName());
        }
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }

}

package com.example.onlineshop.repository.product;

import com.example.onlineshop.criteria.product.BrandCriteria;
import com.example.onlineshop.domain.product.Brand;
import com.example.onlineshop.repository.IGenericRepository;

public interface IBrandRepository extends IGenericRepository<Brand, BrandCriteria> {

    boolean existByBrandName(String name);
}

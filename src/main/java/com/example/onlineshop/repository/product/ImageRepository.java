package com.example.onlineshop.repository.product;

import com.example.onlineshop.domain.product.Image;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Repository for {@link Image}
 */
public interface ImageRepository extends JpaRepository<Image, Long> {

    Optional<Image> findByName(String hashName);


}

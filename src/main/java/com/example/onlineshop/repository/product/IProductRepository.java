package com.example.onlineshop.repository.product;

import com.example.onlineshop.criteria.product.ProductCriteria;
import com.example.onlineshop.domain.product.Product;
import com.example.onlineshop.repository.IGenericRepository;

public interface IProductRepository extends IGenericRepository<Product, ProductCriteria> {

}

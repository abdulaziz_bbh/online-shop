package com.example.onlineshop.repository.product.impl;

import com.example.onlineshop.criteria.product.CategoryCriteria;
import com.example.onlineshop.domain.product.Category;
import com.example.onlineshop.repository.GenericDao;
import com.example.onlineshop.repository.product.ICategoryRepository;
import com.example.onlineshop.util.BaseUtils;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Repository for {@link Category}
 */
@Repository
public class CategoryRepository extends GenericDao<Category, CategoryCriteria> implements ICategoryRepository {

    @PersistenceContext
    private final EntityManager entityManager;

    protected CategoryRepository(BaseUtils utils, EntityManager entityManager) {
        super(utils);
        this.entityManager = entityManager;
    }

    @Override
    public boolean existCategoryByName(String name) {
            return (boolean)entityManager.createQuery("select coalesce((select exists (select 1 from Category b where b.name = :name) as exist_row from Category group by exist_row), false)")
                    .setParameter("name", name)
                    .getSingleResult();
        }

    @Override
    protected void defineCriteriaOnQuery(
            CategoryCriteria criteria,
            List<String> whereCause,
            Map<String, Object> params,
            StringBuilder queryBuilder) {
        if (!utils.isEmpty(criteria.getId())){
            whereCause.add("t.id = :id");
            params.put("id", criteria.getId());
        }
        if (!utils.isEmpty(criteria.getName())){
            whereCause.add("t.name = :name");
            params.put("name", criteria.getName());
        }
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }
}

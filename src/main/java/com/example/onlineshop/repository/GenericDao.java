package com.example.onlineshop.repository;

import com.example.onlineshop.criteria.GenericCriteria;
import com.example.onlineshop.domain.BaseDomain;
import com.example.onlineshop.enums.State;
import com.example.onlineshop.exception.NotFoundException;
import com.example.onlineshop.util.BaseUtils;
import com.google.common.base.Preconditions;
import jakarta.persistence.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import java.lang.reflect.ParameterizedType;
import java.util.*;

public abstract class GenericDao<T extends BaseDomain, C extends GenericCriteria> {


    protected Class<T> persistentClass;

    @PersistenceContext
    private EntityManager entityManager;

    protected final BaseUtils utils;


    @SuppressWarnings("unchecked")
    protected GenericDao(BaseUtils utils) {
        this.utils = utils;
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }
    public Optional<T> find(Long id) {
        try {
            return Optional.ofNullable(entityManager.createQuery(
                    "select t from " + persistentClass.getSimpleName() + " t where t.state <> 'DELETED' and t.id = " + id, persistentClass).getSingleResult());
        }catch (NoResultException exception) {
            throw  new NotFoundException("Bron-bir natija topilmadi!!!");// todo Message yozib ketishim kerak
        }
    }


    public T find(C criteria) {
        Query query = findInit(criteria, false);
        try {
            return  persistentClass.cast(query.getSingleResult());
        }catch (NoResultException exception) {
            throw  new NoResultException("Natija topilmadi!");// todo Message yozib ketishim kerak
        }
    }

    public Long getTotalCount(C criteria) {
        Query query = findInit(criteria, true);
        return (Long) query.getSingleResult();
    }

    private Query findInit(C criteria, boolean onDefineCount) {
        Query query;
        Map<String, Object> params = new HashMap<>();
        List<String> whereCause = new ArrayList<>();
        StringBuilder queryBuilder = new StringBuilder();

        defineCriteriaOnQuery(criteria, whereCause, params, queryBuilder);

        query = defineQuerySelect(criteria, queryBuilder, onDefineCount);

        defineSetterParams(query, params);

        return  query;
    }
    private void defineSetterParams(Query query, Map<String, Object> params){
        params.keySet().forEach(t -> query.setParameter(t, params.get(t)));
    }

    protected void defineCriteriaOnQuery(C criteria, List<String> whereCause, Map<String, Object> params, StringBuilder queryBuilder) {
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }
    protected void onDefineWhereCause(C criteria, List<String> whereCause, Map<String, Object> params, StringBuilder queryBuilder){
        if (!whereCause.isEmpty()){
            queryBuilder.append(" and ").append(StringUtils.join(whereCause, " and " ));
        }
    }

    protected Query defineQuerySelect(C criteria, StringBuilder queryBuilder, boolean onDefineCount){
        String queryString = "select " + (onDefineCount ? " count(t) " : " t ") + " from " + persistentClass.getSimpleName() + " t " +
                joinStringOnQuering(criteria) +
                " where t.state <> 'DELETED' " + queryBuilder.toString() + (onDefineCount ? "" : onSortBy(criteria).toString());
        return onDefineCount ? entityManager.createQuery(queryString, Long.class) : entityManager.createQuery(queryString);
    }
    protected StringBuilder joinStringOnQuering(C criteria){
        return new StringBuilder();
    }
    protected StringBuilder onSortBy(C criteria){
        StringBuilder sorting = new StringBuilder();
        if (!utils.isEmpty(criteria.getSortBy())){
            String ascDec = criteria.getSortDirection();
            sorting.append(" order by ").append("t.").append(criteria.getSortBy()).append(" ").append(ascDec);
        }
        return sorting;
    }


    public List<T> findAll(C criteria) {
        return findAllGeneric(criteria);
    }

    protected <G> List<G> findAllGeneric(C criteria){
        Query query = findInit(criteria, false);
        return getResult(criteria, query);
    }

    @SuppressWarnings("unchecked")
    protected <G> List<G> getResult(C criteria, Query query){
        if ((criteria.getPage() == null || criteria.getPerPage() == null) || (criteria.getPerPage() < 0 || criteria.getPerPage() <= 0)){
            return query.getResultList();
        }else {
            return query.setFirstResult(criteria.getPage() * criteria.getPerPage()).setMaxResults(criteria.getPerPage()).getResultList();
        }
    }

    public boolean existById(Long id) {
        return !((boolean) entityManager.createQuery("select exists (select t from " + persistentClass.getSimpleName() + " t where t.id = :id) from " + persistentClass.getSimpleName())
                .setParameter("id", id)
                .getSingleResult());
    }
    public void save(T entity) {
        Preconditions.checkNotNull(entity);
        getCurrentSession().persist(entity);
    }

    public T update(T entity) {
        Preconditions.checkNotNull(entity);
        entity.setState(State.UPDATED);
        getCurrentSession().merge(entity);
        return entity;
    }

    public void delete(T entity) {
        Preconditions.checkNotNull(entity);
        entity.setState(State.DELETED);
        save(entity);
    }

    protected Session getCurrentSession() {
        return entityManager.unwrap(Session.class);
    }
}


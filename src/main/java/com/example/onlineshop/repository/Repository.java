package com.example.onlineshop.repository;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface Repository {
}

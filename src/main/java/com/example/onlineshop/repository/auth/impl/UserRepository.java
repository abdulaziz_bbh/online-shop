package com.example.onlineshop.repository.auth.impl;

import com.example.onlineshop.criteria.auth.UserCriteria;
import com.example.onlineshop.domain.auth.User;
import com.example.onlineshop.repository.GenericDao;
import com.example.onlineshop.repository.auth.IUserRepository;
import com.example.onlineshop.util.BaseUtils;
import com.example.onlineshop.util.MessageConstant;
import com.example.onlineshop.util.MessageService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class UserRepository extends GenericDao<User, UserCriteria> implements IUserRepository {

    @PersistenceContext
    private final EntityManager entityManager;

    protected UserRepository(BaseUtils utils, EntityManager entityManager) {
        super(utils);
        this.entityManager = entityManager;
    }


    @Override
    protected void defineCriteriaOnQuery(
           UserCriteria criteria,
           List<String> whereCause,
           Map<String, Object> params,
           StringBuilder queryBuilder){
        if (!utils.isEmpty(criteria.getId())){
            whereCause.add("t.id = :id");
            params.put("id", criteria.getId());
        }
        if ((!utils.isEmpty(criteria.getEmail()))){
            whereCause.add("t.email = :email");
            params.put("email", criteria.getEmail());
        }
        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }


    public Optional<User> findByUsername(String username) {
        try {
            return Optional.ofNullable((User) entityManager.createQuery("select t from  User t where t.email = :username")
                    .setParameter("username", username)
                    .getSingleResult());
        }catch (NoResultException e){
            throw new NoResultException(MessageService.getMessage(MessageConstant.USERNAME_NOT_FOUND));
        }
    }
    public boolean existByUsername(String username) {
        return (boolean) entityManager.createQuery("select exists (select t from User t where t.email = :username) from User")
                .setParameter("username", username)
                .getSingleResult();
    }

}

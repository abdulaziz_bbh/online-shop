package com.example.onlineshop.repository.auth;

import com.example.onlineshop.criteria.auth.UserCriteria;
import com.example.onlineshop.domain.auth.User;
import com.example.onlineshop.repository.IGenericRepository;
import com.example.onlineshop.repository.Repository;

import java.util.Optional;

public interface IUserRepository extends IGenericRepository<User, UserCriteria> {

    Optional<User> findByUsername(String username);

    boolean existByUsername(String username);





}

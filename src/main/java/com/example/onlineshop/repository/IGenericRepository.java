package com.example.onlineshop.repository;

import com.example.onlineshop.criteria.GenericCriteria;
import com.example.onlineshop.domain.BaseDomain;

import java.util.List;
import java.util.Optional;

public interface IGenericRepository<T extends BaseDomain, C extends GenericCriteria> extends Repository{

    Optional<T> find(Long id);

    T find(C criteria);

    Long getTotalCount(C criteria);

    List<T> findAll(C criteria);

    void save(T entity);

    T update(T entity);

    void delete(T entity);

    boolean existById(Long id);

}

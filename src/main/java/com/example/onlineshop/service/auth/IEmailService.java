package com.example.onlineshop.service.auth;

import com.example.onlineshop.dto.auth.EmailDto;

public interface IEmailService {

    void sendSimpleMessage(EmailDto emailDto);
}

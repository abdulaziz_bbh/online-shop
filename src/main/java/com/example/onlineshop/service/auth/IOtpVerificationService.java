package com.example.onlineshop.service.auth;

import com.example.onlineshop.dto.auth.OtpVerificationRequest;
import com.example.onlineshop.response.ResponseData;

public interface IOtpVerificationService {

    ResponseData<Boolean> sendOtp(String key);

    ResponseData<Boolean> verifyOtp(OtpVerificationRequest request);

    Boolean validateOtp(String key, Integer otpNumber);
}

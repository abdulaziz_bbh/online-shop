package com.example.onlineshop.service.auth;

import com.example.onlineshop.criteria.auth.UserCriteria;
import com.example.onlineshop.domain.auth.User;
import com.example.onlineshop.dto.auth.AuthenticationRequest;
import com.example.onlineshop.dto.auth.UserCreateDto;
import com.example.onlineshop.dto.auth.UserDto;
import com.example.onlineshop.dto.auth.UserUpdateDto;
import com.example.onlineshop.dto.auth.AuthenticationResponse;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.IGenericCrudService;

public interface IUserService extends IGenericCrudService<
        User,
        UserDto,
        UserCreateDto,
        UserUpdateDto,
        UserCriteria,
        Long> {

    ResponseData<UserDto> findByUsername(String username);

    void baseValidation(User entity);

    ResponseData<AuthenticationResponse> authenticate(AuthenticationRequest request);

    void saveUserToken(User user, String jwtToken);

    void revokeAllUserTokens(User user);

    void existEmail(String email);

    ResponseData<UserDto> find(UserCriteria criteria);
}

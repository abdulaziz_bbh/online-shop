package com.example.onlineshop.service.auth.impl;

import com.example.onlineshop.service.auth.IOtpGenerator;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Service
public class OtpGenerator implements IOtpGenerator {

    private static final Integer EXPIRE_MIN = 5;

    private final LoadingCache<String, Integer> otpCache;

    public OtpGenerator(){
        super();
        otpCache = CacheBuilder.newBuilder()
                .expireAfterWrite(EXPIRE_MIN, TimeUnit.MINUTES)
                .build(new CacheLoader<>() {
                    @Override
                    @NonNull
                    public Integer load(@NonNull String key){
                        return generateOTP(key);
                    }
                });
    }
    @Override
    public Integer generateOTP(String key) {
        Random random = new Random();
        int otp = 100000 + random.nextInt(900000);
        otpCache.put(key, otp);
        return otp;
    }

    @Override
    public Integer getOTPByKey(String key) {
        return otpCache.getIfPresent(key);
    }

    @Override
    public void clearOTPFromCache(String key) {
        otpCache.invalidate(key);
    }
}

package com.example.onlineshop.service.auth.impl;

import com.example.onlineshop.dto.auth.EmailDto;
import com.example.onlineshop.service.auth.IEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService implements IEmailService {

    private final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    private final JavaMailSender javaMailSender;

    public EmailService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendSimpleMessage(EmailDto emailDto) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(String.join(",", emailDto.getRecipients()));
        message.setSubject(emailDto.getSubject());
        message.setText(emailDto.getBody());
        try {
            javaMailSender.send(message);
        }catch (RuntimeException e){
            LOGGER.error("Sending email error {}", e.getMessage());
            throw new RuntimeException("Sending email error");
        }


    }
}

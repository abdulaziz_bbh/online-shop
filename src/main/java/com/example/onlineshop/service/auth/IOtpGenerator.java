package com.example.onlineshop.service.auth;

public interface IOtpGenerator {

    Integer generateOTP(String key);

    Integer getOTPByKey(String key);

    void clearOTPFromCache(String key);
}

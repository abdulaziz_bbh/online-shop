package com.example.onlineshop.service.auth;

import com.example.onlineshop.exception.ValidException;
import com.example.onlineshop.repository.auth.IUserRepository;
import com.example.onlineshop.util.MessageConstant;
import com.example.onlineshop.util.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username.isBlank() || username.isEmpty()){
            throw new ValidException(MessageService.getMessage(MessageConstant.THIS_FIELD_CANNOT_BE_NULL));
        }
        return userRepository.findByUsername(username).orElseThrow(
                () -> new UsernameNotFoundException(MessageService.getMessage(
                        MessageConstant.USERNAME_NOT_FOUND
                ))
        );
    }
}

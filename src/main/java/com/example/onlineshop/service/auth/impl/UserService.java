package com.example.onlineshop.service.auth.impl;

import com.example.onlineshop.criteria.auth.UserCriteria;
import com.example.onlineshop.domain.auth.Role;
import com.example.onlineshop.domain.auth.Token;
import com.example.onlineshop.domain.auth.User;
import com.example.onlineshop.dto.auth.AuthenticationRequest;
import com.example.onlineshop.dto.auth.UserCreateDto;
import com.example.onlineshop.dto.auth.UserDto;
import com.example.onlineshop.dto.auth.UserUpdateDto;
import com.example.onlineshop.enums.State;
import com.example.onlineshop.exception.RestException;
import com.example.onlineshop.exception.ValidException;
import com.example.onlineshop.mapper.auth.UserMapper;
import com.example.onlineshop.repository.auth.IUserRepository;
import com.example.onlineshop.dto.auth.AuthenticationResponse;
import com.example.onlineshop.repository.auth.TokenRepository;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.security.JwtService;
import com.example.onlineshop.service.auth.IUserService;
import com.example.onlineshop.util.BaseUtils;
import com.example.onlineshop.util.MessageConstant;
import com.example.onlineshop.util.MessageService;
import com.example.onlineshop.util.RestConstant;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Service for {@link User}
 */

@Service
@RequiredArgsConstructor
@Transactional
public class UserService implements IUserService {

    private final AuthenticationManager authenticationManager;
    private final IUserRepository userRepository;
    private final UserMapper userMapper;
    private final JwtService jwtService;
    private final TokenRepository tokenRepository;
    private final BaseUtils utils;

    @Override
    public ResponseData<UserDto> get(Long id) {
        User user = userRepository.find(id)
                .orElseThrow(() -> new ValidException(
                        MessageService.getMessage(
                                MessageConstant.NO_CONTENT)));

        return ResponseData.successResponse(userMapper.toDto(user));
    }

    @Override
    public ResponseData<UserDto> findByUsername(String username){
        User user = userRepository.findByUsername(username)
                .orElseThrow(
                        () -> new UsernameNotFoundException(
                                MessageService.getMessage(
                                        MessageConstant.USERNAME_NOT_FOUND)));

        return ResponseData.successResponse(userMapper.toDto(user));
    }

    @Override
    public ResponseData<List<UserDto>> getAll(UserCriteria criteria) {

        return null;
    }

    @Override
    public ResponseData<UserDto> create(UserCreateDto createDto) {
        existEmail(createDto.getEmail());
        User newUser = userMapper.fromCreateDto(createDto);
        baseValidation(newUser);
        newUser.setRole(Role.USER);
        userRepository.save(newUser);
        return ResponseData.successResponse(userMapper.toDto(newUser));
    }
    @Override
    public ResponseData<UserDto> update(Long id, UserUpdateDto updateDto) {
        if (utils.isEmpty(id)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL
                    )
            );
        }
        User user = userRepository.find(id)
                .orElseThrow(
                        () -> RestException.restThrow(
                                MessageService.getMessage(
                                        MessageConstant.NO_CONTENT)));
        user = userMapper.fromUpdateDto(updateDto, user);
        userRepository.update(user);
        return ResponseData.successResponse(userMapper.toDto(user));
    }

    @Override
    public ResponseData<UserDto> delete(Long aLong) {
        return null;
    }

    @Override
    public void baseValidation(User entity) {
        if (entity.getUsername().isEmpty() || entity.getUsername().isBlank())
            throw new ValidException(MessageService.getMessage(MessageConstant.THIS_FIELD_CANNOT_BE_NULL));
        if (entity.getPassword().isEmpty() || entity.getPassword().isBlank())
            throw new ValidException(MessageService.getMessage(MessageConstant.THIS_FIELD_CANNOT_BE_NULL));
        if (entity.getState() == null){
            entity.setState(State.NEW);
        }
    }
    @Override
    public ResponseData<AuthenticationResponse> authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        User user = userRepository.findByUsername(request.getEmail())
                .orElseThrow();
        String jwtToken = jwtService.generateAccessToken(user);
        String refreshToken = jwtService.generateRefreshToken(user);
        revokeAllUserTokens(user);
        saveUserToken(user, jwtToken);
        AuthenticationResponse authenticationResponse = AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();
        return ResponseData.successResponse(authenticationResponse);
    }

    @Override
    public void saveUserToken(User user, String jwtToken) {
        var token = Token.builder()
                .user(user)
                .token(jwtToken)
                .tokenType(RestConstant.TOKEN_TYPE)
                .expired(false)
                .revoked(false)
                .build();
        tokenRepository.save(token);
    }

    @Override
    public void revokeAllUserTokens(User user) {
        List<Token> validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }

    @Override
    public void existEmail(String email) {
        if (email.isEmpty() || email.isBlank())
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL));

         if(userRepository.existByUsername(email)){
             throw new ValidException(
                     MessageService.getMessage(
                             MessageConstant.USER_ALREADY_REGISTERED));
         }
    }
    @Override
    public ResponseData<UserDto> find(UserCriteria criteria) {
        if (criteria == null){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL));
        }
        User user = userRepository.find(criteria);
        UserDto reponse = userMapper.toDto(user);
        return ResponseData.successResponse(reponse);
    }

}

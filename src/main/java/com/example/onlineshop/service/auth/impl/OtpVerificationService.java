package com.example.onlineshop.service.auth.impl;

import com.example.onlineshop.domain.auth.User;
import com.example.onlineshop.dto.auth.EmailDto;
import com.example.onlineshop.dto.auth.OtpVerificationRequest;
import com.example.onlineshop.exception.RestException;
import com.example.onlineshop.repository.auth.IUserRepository;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.auth.IEmailService;
import com.example.onlineshop.service.auth.IOtpGenerator;
import com.example.onlineshop.service.auth.IOtpVerificationService;
import com.example.onlineshop.util.MessageConstant;
import com.example.onlineshop.util.MessageService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OtpVerificationService implements IOtpVerificationService {

    private final IEmailService emailService;
    private final IOtpGenerator otpGenerator;
    private final IUserRepository userRepository;

    private final Logger LOGGER = LoggerFactory.getLogger(OtpVerificationService.class);

    public void sendingGenerateOtp(String key) {

        Integer otpValue = otpGenerator.generateOTP(key);
        if (otpValue == -1){
            LOGGER.error("OTP generator not working...");
            throw RestException.restThrow(MessageService.getMessage(
                    MessageConstant.OTP_GENERATOR_NOT_WORKING));
        }
        LOGGER.info("Generate otp : {}", otpValue);

        User user = userRepository.findByUsername(key)
                .orElseThrow(() -> new UsernameNotFoundException(
                        MessageService.getMessage(
                                MessageConstant.USERNAME_NOT_FOUND)));
        List<String> recipients = new ArrayList<>();
        recipients.add(user.getEmail());

        EmailDto emailDto = new EmailDto();
        emailDto.setSubject("OTP for verification email.");
        emailDto.setBody("OTP password: "+ otpValue);
        emailDto.setRecipients(recipients);

        emailService.sendSimpleMessage(emailDto);
    }

    @Override
    @Transactional
    public ResponseData<Boolean> sendOtp(String email) {
        if (!email.isBlank() || !email.isEmpty()){
            sendingGenerateOtp(email);
            return new ResponseData<>(true); // todo optimalroq yechib berib ketish kerak
        }
        else {
            throw RestException.restThrow(MessageService.getMessage(
                    MessageConstant.OTP_NOT_SENDING));
        }
    }

    @Override
    @Transactional
    public ResponseData<Boolean> verifyOtp(OtpVerificationRequest request) {
        return new ResponseData<>(validateOtp(request.getEmail(), request.getOtp()));
    }

    @Override
    public Boolean validateOtp(String key, Integer otpNumber) {
        Integer cacheOTP = otpGenerator.getOTPByKey(key);
        User user = userRepository.findByUsername(key)
                .orElseThrow(() ->
                        new UsernameNotFoundException(
                                MessageService.getMessage(
                                        MessageConstant.USERNAME_NOT_FOUND)));
        if (cacheOTP != null && cacheOTP.equals(otpNumber)){
            otpGenerator.clearOTPFromCache(key);
            user.setEnabled(true);
            userRepository.save(user);
            return true; // todo optimalroq yechib berib ketish kerak
        }
        else {
            throw RestException.restThrow(MessageService.getMessage(
                MessageConstant.OTP_NOT_MACHED));
        }
    }
}
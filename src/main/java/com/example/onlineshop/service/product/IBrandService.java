package com.example.onlineshop.service.product;

import com.example.onlineshop.criteria.product.BrandCriteria;
import com.example.onlineshop.domain.product.Brand;
import com.example.onlineshop.dto.product.BrandCreateDto;
import com.example.onlineshop.dto.product.BrandDto;
import com.example.onlineshop.dto.product.BrandUpdateDto;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.IGenericCrudService;

public interface IBrandService extends IGenericCrudService<
        Brand,
        BrandDto,
        BrandCreateDto,
        BrandUpdateDto,
        BrandCriteria,
        Long> {

    void exist(Long id);

    void existByBrandName(String name);

    ResponseData<BrandDto> attachImage(Long brandId, String imageId);
}

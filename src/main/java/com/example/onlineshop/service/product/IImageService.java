package com.example.onlineshop.service.product;

import com.example.onlineshop.dto.product.ImageDto;
import com.example.onlineshop.response.ResponseData;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;


public interface IImageService {

    ResponseData<ImageDto> save(MultipartFile multipartFile);

    ResponseData<ImageDto> findByName(String hashName);

    ResponseData<ImageDto> findById(Long id);

    byte[] previewImageByName(String hashName);
}

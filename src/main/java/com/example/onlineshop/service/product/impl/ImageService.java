package com.example.onlineshop.service.product.impl;

import com.example.onlineshop.domain.product.Image;
import com.example.onlineshop.dto.product.ImageDto;
import com.example.onlineshop.enums.State;
import com.example.onlineshop.exception.NotFoundException;
import com.example.onlineshop.exception.RestException;
import com.example.onlineshop.exception.ValidException;
import com.example.onlineshop.mapper.product.ImageMapper;
import com.example.onlineshop.repository.product.ImageRepository;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.product.IImageService;
import com.example.onlineshop.util.BaseUtils;
import com.example.onlineshop.util.MessageConstant;
import com.example.onlineshop.util.MessageService;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.io.IOUtils;
import org.hashids.Hashids;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import java.io.*;
import java.time.LocalDate;

/**
 * Service for {@link com.example.onlineshop.domain.product.Image}
 */

@Service
public class ImageService implements IImageService {

    @Value("${online-shop.image.upload.folder}")
    private String uploadFolderPath;

    private final ImageRepository imageRepository;
    private final BaseUtils utils;
    private final Hashids hashids;
    private final ImageMapper imageMapper;

    Logger LOGGER = LoggerFactory.getLogger(ImageService.class);

    public ImageService(ImageRepository imageRepository, BaseUtils utils, ImageMapper imageMapper) {
        this.imageRepository = imageRepository;
        this.utils = utils;
        this.imageMapper = imageMapper;
        this.hashids = new Hashids(getClass().getName(), 12);
    }

    @Override
    public ResponseData<ImageDto> save(MultipartFile multipartFile) {
        Image image = saveDraftImage(multipartFile);
        return ResponseData.successResponse(createHashIdAsImageName(image.getId(), multipartFile));
    }

    @Transactional(readOnly = true)
    @Override
    public ResponseData<ImageDto> findByName(@NotNull String hashName) {
        Image image = imageRepository.findByName(hashName)
                .orElseThrow(() -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.IMAGE_NOT_FOUND
                        )
                ));
        return ResponseData.successResponse(imageMapper.toDto(image));
    }

    @Transactional(readOnly = true)
    @Override
    public ResponseData<ImageDto> findById(Long id) {
        Image image = imageRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.IMAGE_NOT_FOUND
                        )
                ));
        return ResponseData.successResponse(imageMapper.toDto(image));
    }

    @Override
    public byte[] previewImageByName(String hashName)  {
        Image image = imageRepository.findByName(hashName)
                .orElseThrow(
                        () -> new NotFoundException(
                                MessageService.getMessage(
                                        MessageConstant.IMAGE_NOT_FOUND
                                )
                        )
                );
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(image.getUploadPath());
            return IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            throw RestException.restThrow(e.getMessage());
        }
    }

    private Image saveDraftImage(MultipartFile multipartFile){
        Image newImage = new Image();
        newImage.setExtansion(getExt(multipartFile.getOriginalFilename()));
        newImage.setFileSize(multipartFile.getSize());
        newImage.setContentType(multipartFile.getContentType());
        newImage.setState(State.DRAFT);

        return imageRepository.save(newImage);
    }
    private ImageDto createHashIdAsImageName(Long id, MultipartFile multipartFile){
        File folder = new File(String.format("%s/%s", this.uploadFolderPath, getUploadFolderNameOfDay()));
       if(mkdirs(folder)){
         LOGGER.info("create folder ------------------------: name :  "+folder.getName());
       }

        Image image = imageRepository.findById(id).orElseThrow(
                () -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.IMAGE_NOT_FOUND
                        ))
        );
       image.setName(getNewImageName(id));
        image.setUploadPath(folder.getPath()+"\\"+image.getName()+"."+image.getExtansion());
        folder = folder.getAbsoluteFile();
        File newImage = new File(folder, image.getName()+"."+image.getExtansion());

        try {
            multipartFile.transferTo(newImage);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        imageRepository.save(image);

        return imageMapper.toDto(image);
    }
    private String getNewImageName(Long id){
        return hashids.encode(id);
    }
    private String getUploadFolderNameOfDay(){
        return LocalDate.now().toString();
    }

    private boolean mkdirs(File folder){
        return !folder.exists() && folder.mkdirs();
    }
    private String getExt(String imageName){
        if (!utils.isEmpty(imageName) && !imageName.isEmpty()){
            int dot = imageName.lastIndexOf('.');
            if (dot > 0 && dot <= imageName.length() - 2){
                return imageName.substring(dot + 1);
            }
        }
        throw new ValidException(
                MessageService.getMessage(
                        MessageConstant.THIS_FIELD_CANNOT_BE_NULL
        ));
    }
}

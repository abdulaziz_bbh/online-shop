package com.example.onlineshop.service.product;

import com.example.onlineshop.criteria.product.ProductCriteria;
import com.example.onlineshop.domain.product.Product;
import com.example.onlineshop.dto.product.ProductCreateDto;
import com.example.onlineshop.dto.product.ProductDto;
import com.example.onlineshop.dto.product.ProductUpdateDto;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.IGenericCrudService;

public interface IProductService extends IGenericCrudService<
        Product,
        ProductDto,
        ProductCreateDto,
        ProductUpdateDto,
        ProductCriteria,
        Long> {

    void exist(Long id);

  //  ResponseData<ProductDto> attachProperty(Long propertyId, Long productId);

    ResponseData<ProductDto> attachImage(String imageHashId, Long productId);
}

package com.example.onlineshop.service.product.impl;

import com.example.onlineshop.criteria.product.PropertyCriteria;
import com.example.onlineshop.domain.product.Property;
import com.example.onlineshop.dto.product.PropertyCreateDto;
import com.example.onlineshop.dto.product.PropertyDto;
import com.example.onlineshop.dto.product.PropertyUpdateDto;
import com.example.onlineshop.exception.NotFoundException;
import com.example.onlineshop.exception.ValidException;
import com.example.onlineshop.mapper.product.PropertyMapper;
import com.example.onlineshop.repository.product.IPropertyRepository;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.product.IPropertyService;
import com.example.onlineshop.util.BaseUtils;
import com.example.onlineshop.util.MessageConstant;
import com.example.onlineshop.util.MessageService;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Service for {@link Property}
 */

@Service
@Transactional
public class PropertyService implements IPropertyService {

    private final PropertyMapper propertyMapper;
    private final BaseUtils utils;
    private final IPropertyRepository propertyRepository;

    public PropertyService(PropertyMapper propertyMapper, BaseUtils utils, IPropertyRepository propertyRepository) {
        this.propertyMapper = propertyMapper;
        this.utils = utils;
        this.propertyRepository = propertyRepository;
    }

    @Override
    public ResponseData<PropertyDto> get(@NotNull Long id) {
        Property property = propertyRepository.find(id)
                .orElseThrow(
                        () -> new NotFoundException(
                                MessageService.getMessage(
                                        MessageConstant.NO_CONTENT)
                ));
        PropertyDto response = propertyMapper.toDto(property);
        return ResponseData.successResponse(response);
    }

    @Override
    public ResponseData<List<PropertyDto>> getAll(@NotNull PropertyCriteria criteria) {
        List<Property> resultList = propertyRepository.findAll(criteria);
        if (utils.isEmpty(resultList)){
            throw new NotFoundException(
                    MessageService.getMessage(
                            MessageConstant.NO_CONTENT));
        }
        List<PropertyDto> response = propertyMapper.toDto(resultList);
        return ResponseData.successResponse(response);
    }

    @Override
    public ResponseData<PropertyDto> create(PropertyCreateDto createDto) {
        if (utils.isEmpty(createDto)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL
                    )
            );
        }
        Property property = propertyMapper.fromCreateDto(createDto);
        propertyRepository.save(property);
        PropertyDto response = propertyMapper.toDto(property);
        return ResponseData.successResponse(response);
    }

    @Override
    public ResponseData<PropertyDto> update(@NotNull Long id, PropertyUpdateDto updateDto) {
        exist(id);
        Property property = propertyRepository.find(id)
                .orElseThrow(
                        () -> new NotFoundException(
                                MessageService.getMessage(
                                        MessageConstant.NO_CONTENT)));
        property = propertyMapper.fromUpdateDto(updateDto, property);
        propertyRepository.update(property);
        return ResponseData.successResponse(propertyMapper.toDto(property));
    }

    @Override
    public ResponseData<PropertyDto> delete(Long id) {
        exist(id);
        Property property = propertyRepository.find(id)
                .orElseThrow(
                        () -> new NotFoundException(
                                MessageService.getMessage(
                                        MessageConstant.NO_CONTENT)));
        propertyRepository.delete(property);
        return ResponseData.successResponse(propertyMapper.toDto(property));
    }

    private void exist(Long id) {
        if (utils.isEmpty(id)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL
                    )
            );
        }
        if (propertyRepository.existById(id)){
            throw new NotFoundException(
                    MessageService.getMessage(
                            MessageConstant.NO_CONTENT
                    )
            );
        }
    }
}

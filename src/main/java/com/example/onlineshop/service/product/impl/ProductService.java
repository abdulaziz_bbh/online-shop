package com.example.onlineshop.service.product.impl;

import com.example.onlineshop.criteria.product.ProductCriteria;
import com.example.onlineshop.domain.product.Image;
import com.example.onlineshop.domain.product.Product;
import com.example.onlineshop.domain.product.Property;
import com.example.onlineshop.dto.product.ProductCreateDto;
import com.example.onlineshop.dto.product.ProductDto;
import com.example.onlineshop.dto.product.ProductUpdateDto;
import com.example.onlineshop.enums.State;
import com.example.onlineshop.exception.NotFoundException;
import com.example.onlineshop.exception.ValidException;
import com.example.onlineshop.mapper.product.ProductMapper;
import com.example.onlineshop.repository.product.IProductRepository;
import com.example.onlineshop.repository.product.IPropertyRepository;
import com.example.onlineshop.repository.product.ImageRepository;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.product.IProductService;
import com.example.onlineshop.util.BaseUtils;
import com.example.onlineshop.util.MessageConstant;
import com.example.onlineshop.util.MessageService;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Collections;
import java.util.List;

/**
 * Service for {@link Product}
 */

@Service
@Transactional
public class ProductService implements IProductService {

    private final IProductRepository productRepository;
    private final IPropertyRepository propertyRepository;
    private final ImageRepository imageRepository;
    private final ProductMapper productMapper;
    private final BaseUtils utils;

    public ProductService(IProductRepository productRepository, IPropertyRepository propertyRepository, ImageRepository imageRepository, ProductMapper productMapper, BaseUtils utils) {
        this.productRepository = productRepository;
        this.propertyRepository = propertyRepository;
        this.imageRepository = imageRepository;
        this.productMapper = productMapper;
        this.utils = utils;
    }

    @Override
    public ResponseData<ProductDto> get(@NotNull Long id) {
        exist(id);
        Product product = productRepository.find(id)
                .orElseThrow(() -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.PRODUCT_NOT_FOUND)));
        return ResponseData.successResponse(productMapper.toDto(product));
    }

    @Override
    public ResponseData<List<ProductDto>> getAll(ProductCriteria criteria) {
        List<Product> result = productRepository.findAll(criteria);
        if (utils.isEmpty(result)){
            throw new NotFoundException(
                    MessageService.getMessage(
                            MessageConstant.NO_CONTENT));
        }
        List<ProductDto> response = productMapper.toDto(result);
        return ResponseData.successResponse(response);
    }

    @Override
    public ResponseData<ProductDto> create(ProductCreateDto createDto) {
        if (utils.isEmpty(createDto)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL));
        }
        Product product = productMapper.fromCreateDto(createDto);
        productRepository.save(product);
        return ResponseData.successResponse(productMapper.toDto(product));
    }
    @Override
    public ResponseData<ProductDto> update(@NotNull Long id, ProductUpdateDto updateDto) {
        exist(id);
        Product product = productRepository.find(id)
                .orElseThrow(() -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.PRODUCT_NOT_FOUND)));
        product = productMapper.fromUpdateDto(updateDto, product);

        productRepository.update(product);
        return ResponseData.successResponse(productMapper.toDto(product));
    }
    @Override
    public ResponseData<ProductDto> delete(@NotNull Long id) {
        exist(id);
        Product product = productRepository.find(id)
                .orElseThrow(() -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.PRODUCT_NOT_FOUND)));
        productRepository.delete(product);
        return ResponseData.successResponse(productMapper.toDto(product));
    }

    @Override
    public void exist(Long id) {
        if (utils.isEmpty(id)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL));
        }
        if (productRepository.existById(id)){
            throw new NotFoundException(
                    MessageService.getMessage(
                            MessageConstant.NO_CONTENT));
        }
    }

//    @Override
//    public ResponseData<ProductDto> attachProperty(@NotNull Long propertyId, Long productId) {
//        Property property = propertyRepository.find(productId).orElseThrow(
//                () -> new NotFoundException(MessageService.getMessage(
//                        MessageConstant.PROPERTY_NOT_FOUND)));
//        Product product = productRepository.find(productId).orElseThrow(
//                () -> new NotFoundException(MessageService.getMessage(
//                        MessageConstant.PRODUCT_NOT_FOUND)));
//        product.setProperty(Collections.singletonList(property));
//        productRepository.save(product);
//        propertyRepository.save(property);
//        return ResponseData.successResponse(productMapper.toDto(product));
//    }

    @Override
    public ResponseData<ProductDto> attachImage(String imageHashId, Long productId) {
        Image image = imageRepository.findByName(imageHashId).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(
                        MessageConstant.IMAGE_NOT_FOUND)));
        Product product = productRepository.find(productId).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(
                        MessageConstant.PRODUCT_NOT_FOUND)));
        product.setImages(Collections.singletonList(image));
        image.setState(State.ACTIVE);
        imageRepository.save(image);
        productRepository.save(product);
        return ResponseData.successResponse(productMapper.toDto(product));
    }
}

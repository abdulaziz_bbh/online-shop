package com.example.onlineshop.service.product.impl;

import com.example.onlineshop.criteria.product.BrandCriteria;
import com.example.onlineshop.domain.product.Brand;
import com.example.onlineshop.domain.product.Image;
import com.example.onlineshop.dto.product.BrandCreateDto;
import com.example.onlineshop.dto.product.BrandDto;
import com.example.onlineshop.dto.product.BrandUpdateDto;
import com.example.onlineshop.enums.State;
import com.example.onlineshop.exception.NotFoundException;
import com.example.onlineshop.exception.ValidException;
import com.example.onlineshop.mapper.product.BrandMapper;
import com.example.onlineshop.repository.product.IBrandRepository;
import com.example.onlineshop.repository.product.ImageRepository;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.product.IBrandService;
import com.example.onlineshop.util.BaseUtils;
import com.example.onlineshop.util.MessageConstant;
import com.example.onlineshop.util.MessageService;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Service for {@link Brand}
 */
@Service
@Transactional
public class BrandService implements IBrandService {

    private final IBrandRepository brandRepository;
    private final BaseUtils utils;
    private final BrandMapper brandMapper;
    private final ImageRepository imageRepository;

    public BrandService(IBrandRepository brandRepository, BaseUtils utils, BrandMapper brandMapper, ImageRepository imageRepository) {
        this.brandRepository = brandRepository;
        this.utils = utils;
        this.brandMapper = brandMapper;
        this.imageRepository = imageRepository;
    }

    @Override
    public ResponseData<BrandDto> get(@NotNull Long id) {
        exist(id);
        Brand brand = brandRepository.find(id)
                .orElseThrow(
                        () -> new NotFoundException(
                                MessageService.getMessage(
                                        MessageConstant.NO_CONTENT)));
        return ResponseData.successResponse(brandMapper.toDto(brand));
    }

    @Override
    public ResponseData<List<BrandDto>> getAll(BrandCriteria criteria) {
        List<Brand> result = brandRepository.findAll(criteria);
        if (utils.isEmpty(result)){
            throw new NotFoundException(
                    MessageService.getMessage(
                            MessageConstant.NO_CONTENT));
        }
        List<BrandDto> response = brandMapper.toDto(result);
        return ResponseData.successResponse(response);
    }

    @Override
    public ResponseData<BrandDto> create(BrandCreateDto createDto) {
        if (utils.isEmpty(createDto)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL));
        }
        existByBrandName(createDto.getName());
        Brand brand = brandMapper.fromCreateDto(createDto);
        brandRepository.save(brand);
        return ResponseData.successResponse(brandMapper.toDto(brand));
    }

    @Override
    public ResponseData<BrandDto> update(@NotNull Long id, BrandUpdateDto updateDto) {
        exist(id);
        Brand brand = brandRepository.find(id)
                .orElseThrow(() -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.NO_CONTENT)));
        brand = brandMapper.fromUpdateDto(updateDto, brand);
        brandRepository.update(brand);
        return ResponseData.successResponse(brandMapper.toDto(brand));
    }

    @Override
    public ResponseData<BrandDto> delete(@NotNull Long id) {
        exist(id);
        Brand brand = brandRepository.find(id)
                .orElseThrow(() -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.NO_CONTENT)));
        brandRepository.delete(brand);
        return ResponseData.successResponse(brandMapper.toDto(brand));
    }


    @Override
    public void exist(@NotNull Long id) {
        if(brandRepository.existById(id)){
            throw new NotFoundException(MessageService.getMessage(
                    MessageConstant.NO_CONTENT));
        }
    }

    @Override
    public void existByBrandName(@NotNull String name) {
        if (utils.isEmpty(name)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL));
        }
        if (brandRepository.existByBrandName(name)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.BRAND_ALREADY_EXISTS));
        }
    }

    @Override
    public ResponseData<BrandDto> attachImage(@NotNull Long brandId, @NotNull String imageHasId) {
        Image image = imageRepository.findByName(imageHasId).orElseThrow(
                () -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.IMAGE_NOT_FOUND)));
        Brand brand = brandRepository.find(brandId).orElseThrow(
                () -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.BRAND_NOT_FOUND)));
        brand.setImage(image);
        image.setState(State.ACTIVE);
        imageRepository.save(image);
        brandRepository.save(brand);
        return ResponseData.successResponse(brandMapper.toDto(brand));
    }
}

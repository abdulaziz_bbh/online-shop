package com.example.onlineshop.service.product.impl;

import com.example.onlineshop.criteria.product.CategoryCriteria;
import com.example.onlineshop.domain.product.Category;
import com.example.onlineshop.domain.product.Image;
import com.example.onlineshop.dto.product.CategoryCreateDto;
import com.example.onlineshop.dto.product.CategoryDto;
import com.example.onlineshop.dto.product.CategoryUpdateDto;
import com.example.onlineshop.enums.State;
import com.example.onlineshop.exception.NotFoundException;
import com.example.onlineshop.exception.ValidException;
import com.example.onlineshop.mapper.product.CategoryMapper;
import com.example.onlineshop.repository.product.ICategoryRepository;
import com.example.onlineshop.repository.product.ImageRepository;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.product.ICategoryService;
import com.example.onlineshop.util.BaseUtils;
import com.example.onlineshop.util.MessageConstant;
import com.example.onlineshop.util.MessageService;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service for {@link Category}
 */

@Service
@Transactional
public class CategoryService implements ICategoryService {

    private final BaseUtils utils;
    private final ICategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;
    private final ImageRepository imageRepository;

    public CategoryService(BaseUtils utils, ICategoryRepository categoryRepository, CategoryMapper categoryMapper, ImageRepository imageRepository) {
        this.utils = utils;
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
        this.imageRepository = imageRepository;
    }

    @Override
    public ResponseData<CategoryDto> get(@NotNull Long id) {
        exist(id);
        Category category = categoryRepository.find(id)
                .orElseThrow(() -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.NO_CONTENT)));
        return ResponseData.successResponse(categoryMapper.toDto(category));
    }

    @Override
    public ResponseData<List<CategoryDto>> getAll(CategoryCriteria criteria) {
        List<Category> result = categoryRepository.findAll(criteria);
        if (utils.isEmpty(result)){
            throw new NotFoundException(
                    MessageService.getMessage(
                            MessageConstant.NO_CONTENT));
        }
        List<CategoryDto> response = categoryMapper.toDto(result);
        return ResponseData.successResponse(response);
    }

    @Override
    public ResponseData<CategoryDto> create(CategoryCreateDto createDto) {
        if (utils.isEmpty(createDto)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL));
        }
        exitByName(createDto.getName());
        Category category = categoryMapper.fromCreateDto(createDto);
        categoryRepository.save(category);
        return ResponseData.successResponse(categoryMapper.toDto(category));
    }

    @Override
    public ResponseData<CategoryDto> update(@NotNull Long id, CategoryUpdateDto updateDto) {
        exist(id);
        Category category = categoryRepository.find(id)
                .orElseThrow(() -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.CATEGORY_NOT_FOUND)));
        category = categoryMapper.fromUpdateDto(updateDto, category);
        categoryRepository.update(category);
        return ResponseData.successResponse(categoryMapper.toDto(category));
    }

    @Override
    public ResponseData<CategoryDto> delete(@NotNull Long id) {
        exist(id);
        Category category = categoryRepository.find(id)
                .orElseThrow(() -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.NO_CONTENT)));
        categoryRepository.delete(category);
        return ResponseData.successResponse(categoryMapper.toDto(category));
    }

    @Override
    public void exist(Long id) {
        if (utils.isEmpty(id)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL));
        }
        if (categoryRepository.existById(id)){
            throw new NotFoundException(
                    MessageService.getMessage(
                            MessageConstant.NO_CONTENT));
        }
    }

    @Override
    public ResponseData<CategoryDto> attachImage(Long categoryId, String imageHashId) {
        Image image = imageRepository.findByName(imageHashId).orElseThrow(
                () -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.IMAGE_NOT_FOUND)));
        Category category = categoryRepository.find(categoryId).orElseThrow(
                () -> new NotFoundException(
                        MessageService.getMessage(
                                MessageConstant.CATEGORY_NOT_FOUND)));
        category.setImage(image);
        image.setState(State.ACTIVE);
        categoryRepository.save(category);
        imageRepository.save(image);
        return ResponseData.successResponse(categoryMapper.toDto(category));
    }

    private void exitByName(String name){
        if (utils.isEmpty(name)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.THIS_FIELD_CANNOT_BE_NULL));
        }
        if (categoryRepository.existCategoryByName(name)){
            throw new ValidException(
                    MessageService.getMessage(
                            MessageConstant.CATEGORY_ALREADY_EXISTS));
        }
    }
}

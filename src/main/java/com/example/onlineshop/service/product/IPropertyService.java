package com.example.onlineshop.service.product;

import com.example.onlineshop.criteria.product.PropertyCriteria;
import com.example.onlineshop.domain.product.Property;
import com.example.onlineshop.dto.product.PropertyCreateDto;
import com.example.onlineshop.dto.product.PropertyDto;
import com.example.onlineshop.dto.product.PropertyUpdateDto;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.IGenericCrudService;

public interface IPropertyService extends IGenericCrudService<
        Property,
        PropertyDto,
        PropertyCreateDto,
        PropertyUpdateDto,
        PropertyCriteria,
        Long> {
}

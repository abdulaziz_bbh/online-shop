package com.example.onlineshop.service.product;

import com.example.onlineshop.criteria.product.CategoryCriteria;
import com.example.onlineshop.domain.product.Category;
import com.example.onlineshop.dto.product.CategoryCreateDto;
import com.example.onlineshop.dto.product.CategoryDto;
import com.example.onlineshop.dto.product.CategoryUpdateDto;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.IGenericCrudService;

public interface ICategoryService extends IGenericCrudService<
        Category,
        CategoryDto,
        CategoryCreateDto,
        CategoryUpdateDto,
        CategoryCriteria,
        Long> {

    void exist(Long id);

    ResponseData<CategoryDto> attachImage(Long categoryId, String imageHashId);
}

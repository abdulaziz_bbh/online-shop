package com.example.onlineshop.service;

import com.example.onlineshop.criteria.Criteria;
import com.example.onlineshop.domain.BaseDomain;
import com.example.onlineshop.dto.Dto;
import com.example.onlineshop.response.ResponseData;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @param <T> entity
 * @param <D> Dto
 * @param <CR> CreateDto
 * @param <UP> UpdateDto
 * @param <C> Criteria
 * @param <ID> Id
 */
public interface IGenericCrudService<
        T extends BaseDomain,
        D extends Dto,
        CR extends Dto,
        UP extends Dto,
        C extends Criteria,
        ID extends Serializable> extends Service{

    ResponseData<D> get(ID id);

    ResponseData<List<D>> getAll(C criteria);

    ResponseData<D> create(@NotNull CR createDto);

    ResponseData<D> update(@NotNull ID id, @NotNull UP updateDto);

    ResponseData<D> delete(@NotNull ID id);


}

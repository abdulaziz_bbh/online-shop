package com.example.onlineshop.controller.user;

import com.example.onlineshop.criteria.auth.UserCriteria;
import com.example.onlineshop.dto.auth.*;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.auth.IOtpVerificationService;
import com.example.onlineshop.service.auth.IUserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/auth")
@RequiredArgsConstructor
public class AuthController {

    private final IUserService userService;
    private final IOtpVerificationService otpVerificationService;

    @PostMapping("sign-up")
    public ResponseEntity<ResponseData<UserDto>> create(
            @Valid
            @RequestBody
            UserCreateDto createDto){
        return ResponseEntity.ok(userService.create(createDto));
    }
    @PostMapping("sign-in")
    public ResponseEntity<ResponseData<AuthenticationResponse>> authenticate(
            @Valid
            @RequestBody
            AuthenticationRequest request){
        return  ResponseEntity.ok(userService.authenticate(request));
    }

    @PostMapping("verify-otp")
    public ResponseEntity<ResponseData<Boolean>> verifyOpt(
            @RequestBody
            @Valid
            OtpVerificationRequest request){
        return ResponseEntity.ok(otpVerificationService.verifyOtp(request));
    }
    @PostMapping("sending-otp/{email}")
    public ResponseEntity<ResponseData<Boolean>> sendingOtp(
            @PathVariable("email")
            String email){
        return ResponseEntity.ok(otpVerificationService.sendOtp(email));
    }
}

package com.example.onlineshop.controller.user;

import com.example.onlineshop.criteria.auth.UserCriteria;
import com.example.onlineshop.dto.auth.UserDto;
import com.example.onlineshop.dto.auth.UserUpdateDto;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.auth.IUserService;
import com.example.onlineshop.util.RestConstant;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(RestConstant.BASE_PATH_V1+"/user")
@RestController
public class UserController {

    private final IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }

    @GetMapping("/find-by-criteria")
    public ResponseEntity<ResponseData<UserDto>> find(
            @Valid
            @RequestBody
            UserCriteria userCriteria){
        return ResponseEntity.ok(userService.find(userCriteria));
    }

    @PutMapping("update-user/{id}")
    public ResponseEntity<ResponseData<UserDto>> update(
            @RequestBody
            UserUpdateDto updateDto,
            @PathVariable("id")
            Long id){
        return ResponseEntity.ok(userService.update(id, updateDto));
    }

}

package com.example.onlineshop.controller.product;

import com.example.onlineshop.criteria.product.BrandCriteria;
import com.example.onlineshop.criteria.product.CategoryCriteria;
import com.example.onlineshop.criteria.product.ProductCriteria;
import com.example.onlineshop.criteria.product.PropertyCriteria;
import com.example.onlineshop.dto.product.*;
import com.example.onlineshop.response.ResponseData;
import com.example.onlineshop.service.product.*;
import com.example.onlineshop.util.RestConstant;
import jakarta.validation.Valid;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@RestController
@RequestMapping(RestConstant.BASE_PATH_V1+"/product")
public class AdminController {

    private final IImageService imageService;
    private final IPropertyService propertyService;
    private final IBrandService brandService;
    private final ICategoryService categoryService;
    private final IProductService productService;

    public AdminController(IImageService imageService, IPropertyService propertyService, IBrandService brandService, ICategoryService categoryService, IProductService productService) {
        this.imageService = imageService;
        this.propertyService = propertyService;
        this.brandService = brandService;
        this.categoryService = categoryService;
        this.productService = productService;
    }

    // image section

    @PostMapping("image/upload")
    public ResponseEntity<ResponseData<ImageDto>> uploadImage(@RequestParam("file") MultipartFile multipartFile){
        return ResponseEntity.ok(imageService.save(multipartFile));
    }

    @GetMapping(
            value = "image/preview/{name}",
            produces = {
                    MediaType.IMAGE_JPEG_VALUE,
                    MediaType.IMAGE_PNG_VALUE
            }
    )
    public @ResponseBody byte[] getImageWithMediaTypeByName(@PathVariable("name") String name){
        return imageService.previewImageByName(name);
    }

    // property section

    @PostMapping("property/create")
    public ResponseEntity<ResponseData<PropertyDto>> createProperty(
            @RequestBody
            @Valid
            PropertyCreateDto dto){
        return ResponseEntity.ok(propertyService.create(dto));
    }

    @PutMapping("property/update/{id}")
    public ResponseEntity<ResponseData<PropertyDto>> updateProperty(
            @RequestBody
            PropertyUpdateDto updateDto,
            @PathVariable("id")
            Long id){
        return ResponseEntity.ok(propertyService.update(id, updateDto));
    }

    @GetMapping("property/find-all")
    public ResponseEntity<ResponseData<List<PropertyDto>>> findAll(
            @RequestBody
            PropertyCriteria criteria){
        return ResponseEntity.ok(propertyService.getAll(criteria));
    }
    @DeleteMapping("property/delete/{id}")
    public ResponseEntity<ResponseData<PropertyDto>> deleteProperty(
            @PathVariable("id")
            Long id){
        return ResponseEntity.ok(propertyService.delete(id));
    }
    @GetMapping("property/get-by-id/{id}")
    public ResponseEntity<ResponseData<PropertyDto>> findPropertyById(
            @PathVariable("id")
            Long id){
        return ResponseEntity.ok(propertyService.get(id));
    }

    // brand section

    @PostMapping("brand/create")
    public ResponseEntity<ResponseData<BrandDto>> createBrand(
            @RequestBody
            @Valid
            BrandCreateDto createDto){
        return ResponseEntity.ok(brandService.create(createDto));
    }
    @PutMapping("brand/update/{id}")
    public ResponseEntity<ResponseData<BrandDto>> updateBrand(
            @PathVariable("id")
            @Valid
            Long id,
            @RequestBody
            BrandUpdateDto updateDto){
        return ResponseEntity.ok(brandService.update(id, updateDto));
    }

    @GetMapping("brand/bet-by-id/{id}")
    public ResponseEntity<ResponseData<BrandDto>> findBrandById(
            @PathVariable("id")
            @Valid
            Long id){
        return ResponseEntity.ok(brandService.get(id));
    }

    @GetMapping("brand/find-all")
    public ResponseEntity<ResponseData<List<BrandDto>>> findAll(
            @RequestBody
            BrandCriteria criteria){
        return ResponseEntity.ok(brandService.getAll(criteria));
    }
    @DeleteMapping("brand/delete/{id}")
    public ResponseEntity<ResponseData<BrandDto>> deleteBrand(
            @PathVariable("id")
            @Valid
            Long id){
        return ResponseEntity.ok(brandService.delete(id));
    }

    @PostMapping("brand/attach-image/{brand-id}/{hash-id}")
    public ResponseEntity<ResponseData<BrandDto>> attachImageToBrand(
            @PathVariable("brand-id")
            @Valid
            Long brandId,
            @PathVariable("hash-id")
            @Valid
            String hashId){
        return ResponseEntity.ok(brandService.attachImage(brandId, hashId));
    }

    // category section

    @PostMapping("category/create")
    public ResponseEntity<ResponseData<CategoryDto>> createCategory(
            @RequestBody
            @Valid
            CategoryCreateDto createDto){
        return ResponseEntity.ok(categoryService.create(createDto));
    }
    @GetMapping("category/find-category-by-id/{id}")
    public ResponseEntity<ResponseData<CategoryDto>> findCategoryById(
            @PathVariable("id")
            @Valid
            Long id){
        return ResponseEntity.ok(categoryService.get(id));
    }
    @GetMapping("category/find-all")
    public ResponseEntity<ResponseData<List<CategoryDto>>> findAll(
            @RequestBody
            CategoryCriteria criteria){
        return ResponseEntity.ok(categoryService.getAll(criteria));
    }
    @PutMapping("category/update/{id}")
    public ResponseEntity<ResponseData<CategoryDto>> updateCategory(
            @PathVariable("id")
            @Valid
            Long id,
            @RequestBody
            CategoryUpdateDto categoryUpdateDto){
        return ResponseEntity.ok(categoryService.update(id, categoryUpdateDto));
    }
    @DeleteMapping("category/delete/{id}")
    public ResponseEntity<ResponseData<CategoryDto>> deleteCategory(
            @PathVariable
            @Valid
            Long id){
        return ResponseEntity.ok(categoryService.delete(id));
    }
    @PostMapping("category/attach-image/{category-id}/{hash-id}")
    public ResponseEntity<ResponseData<CategoryDto>> attachImageToCategory(
            @PathVariable("category-id")
            @Valid
            Long id,
            @PathVariable("hash-id")
            @Valid
            String hashId){
        return ResponseEntity.ok(categoryService.attachImage(id, hashId));
    }

    // product section

    @PostMapping("product/create")
    public ResponseEntity<ResponseData<ProductDto>> createProduct(
            @RequestBody
            @Valid
            ProductCreateDto createDto){
        return ResponseEntity.ok(productService.create(createDto));
    }

    @PutMapping("product/update/{id}")
    public ResponseEntity<ResponseData<ProductDto>> updateProduct(
            @PathVariable("id")
            @Valid
            Long id,
            @RequestBody
            ProductUpdateDto updateDto){
        return ResponseEntity.ok(productService.update(id, updateDto));
    }

    @GetMapping("product/find-product-by-id/{id}")
    public ResponseEntity<ResponseData<ProductDto>> findProductById(
            @PathVariable("id")
            @Valid
            Long id){
        return ResponseEntity.ok(productService.get(id));
    }
    @GetMapping("product/find-all")
    public ResponseEntity<ResponseData<List<ProductDto>>> findAllProduct(
            @RequestBody
            ProductCriteria criteria){
        return ResponseEntity.ok(productService.getAll(criteria));
    }
    @DeleteMapping("product/delete/{id}")
    public ResponseEntity<ResponseData<ProductDto>> deleteProduct(
            @PathVariable("id")
            @Valid
            Long id){
        return ResponseEntity.ok(productService.delete(id));
    }
//    @PostMapping("product/attach-property/{property-id}/{product-id}")
//    public ResponseEntity<ResponseData<ProductDto>> attachPropertyToProduct(
//            @PathVariable("property-id")
//            @Valid
//            Long propertyId,
//            @PathVariable("product-id")
//            @Valid
//            Long productId){
//        return ResponseEntity.ok(productService.attachProperty(propertyId, productId));
//    }
    @PostMapping("product/attach-image/{hash-id}/{product-id}")
    public ResponseEntity<ResponseData<ProductDto>> attachImageToProduct(
            @PathVariable("hash-id")
            @Valid
            String imageHashId,
            @PathVariable("product-id")
            @Valid
            Long productId){
        return ResponseEntity.ok(productService.attachImage(imageHashId, productId));
    }
}
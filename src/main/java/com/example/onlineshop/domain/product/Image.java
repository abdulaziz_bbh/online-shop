package com.example.onlineshop.domain.product;

import com.example.onlineshop.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Image extends BaseDomain {

    @Column(unique = true)
    String name;

    @Column(nullable = false)
    String extansion;

    String uploadPath;

    @Column(nullable = false)
    Long fileSize;

    @Column(nullable = false)
    String contentType;

    @JsonIgnore
    @ManyToOne
    Product product;
}

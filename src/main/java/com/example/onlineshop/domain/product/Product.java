package com.example.onlineshop.domain.product;

import com.example.onlineshop.domain.BaseDomain;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Product extends BaseDomain {

    String name;

    Integer amount;

    Double price;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    List<Property> properties;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    List<Image> images;

    @ManyToOne
    @JoinColumn(name = "brand_id", referencedColumnName = "id")
    Brand brand;

    @ManyToOne
    Category category;

    String description;

    public void setImages(List<Image> images){
        this.images = images;
        for (Image image :images) {
            image.setProduct(this);
        }
    }
    public void setProperty(List<Property> properties){
        this.properties = properties;
        for (Property property : properties){
            property.setProduct(this);
        }
    }
}

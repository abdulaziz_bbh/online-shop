package com.example.onlineshop.domain.product;

import com.example.onlineshop.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Property extends BaseDomain {

    @Column(nullable = false)
    String name;

    @Column(nullable = false)
    String value;

    @JsonIgnore
    @ManyToOne
    Product product;
}

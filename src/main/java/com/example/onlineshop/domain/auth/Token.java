package com.example.onlineshop.domain.auth;

import com.example.onlineshop.domain.BaseDomain;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToOne;
import lombok.*;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
public class Token extends BaseDomain {

    @Column(unique = true)
    String token;

    String tokenType;

    boolean revoked;

    boolean expired;

    @ManyToOne(fetch = FetchType.LAZY)
    User user;
}

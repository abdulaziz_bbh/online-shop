package com.example.onlineshop.enums;

public enum State {
    NEW,
    UPDATED,
    DELETED,
    DRAFT,
    ACTIVE


}

package com.example.onlineshop.validation;

import com.example.onlineshop.dto.auth.UserCreateDto;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        UserCreateDto signUpDto = (UserCreateDto) value;
        return signUpDto.getPassword().equals(signUpDto.getPrePassword());
    }
}

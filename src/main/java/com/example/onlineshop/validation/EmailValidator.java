package com.example.onlineshop.validation;

import com.example.onlineshop.util.RestConstant;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator implements ConstraintValidator<ValidEmail, String> {

    public Pattern pattern;
    public Matcher matcher;


    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        return (validateEmail(email));
    }
    private boolean validateEmail(String email){
        pattern = Pattern.compile(RestConstant.EMAIL_REGEX);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
